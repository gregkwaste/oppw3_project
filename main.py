import struct
import bpy
import bmesh
import imp
import gc
import os
import string_func
from io import BytesIO
from mathutils import Vector
from math import radians
import half

hf = half.Float16Compressor()

# Custom Modules
from string_func import *
from dds30 import *

# Texture PArsing
tex_dict = {0x1: False,
            0x6: ['DXT1', 8],
            0x8: ['DXT5', 16]}
dir = 'I:\\Downloaded Games\\3DMGAME-One.Piece.Pirate.Warriors.3.Incl.DLC.Cracked-3DM\\3DMGAME-One.Piece.Pirate.Warriors.3.Incl.DLC.Cracked-3DM\\OPPW3\\FILE_DX9\\DLC\\EFIGS\\'


def parseTextures(f, name):
    # Clear Path first
    dir = os.path.dirname(name)
    for i in os.listdir(dir):
        if '.dds' in i:
            os.remove(os.path.join(dir, i))
    # Start Parsing
    baseOffset = f.tell()
    identifier = read_string_n(f, 8)
    fileSize = struct.unpack('<I', f.read(4))[0]
    main_offset = struct.unpack('<I', f.read(4))[0]
    textureCount = struct.unpack('<I', f.read(4))[0]
    name = os.path.basename(name)
    name += '_textures'

    texOffsets = []
    sizes = []
    f.seek(baseOffset + main_offset)
    for i in range(textureCount):
        texOffsets.append(struct.unpack('<I', f.read(4))[0] + main_offset)

    for i in range(textureCount - 1):
        sizes.append(texOffsets[i + 1] - texOffsets[i])
    sizes.append(fileSize - texOffsets[-1])

    for i in range(textureCount):
        of = texOffsets[i]
        size = sizes[i]
        if of < fileSize:
            # print 'off: ', of
            f.seek(baseOffset + of)
            # Identify texture
            texinfo = struct.unpack('<4B', f.read(4))
            mipmaps = texinfo[0] >> 4
            mode = texinfo[1]
            width = 2 ** (texinfo[2] & 0x0F)
            height = 2 ** ((texinfo[2] & 0xF0) >> 4)

            head = struct.unpack('<2H', f.read(4))
            if head[1] == 0x21:
                # Decompressed Texture
                size -= 8
            elif head[1] == 0x1021:
                # Compressed Texture
                f.seek(0xC, 1)
                size -= 16
            print(hex(of), hex(size), [mipmaps, mode, width, height])

            # Create DDS File
            t = BytesIO()
            t.write(f.read(size))
            t.seek(0)
            tfile = dds_file(False, t)
            # Set parameters
            tfile.header.dwSize = 0x7C
            tfile.header.dwFlags = 0x20000 + 0x1000 + 0x1 + 0x2 + 0x4
            tfile.header.dwHeight = height
            tfile.header.dwWidth = width
            tfile.header.dwMipMapCount = mipmaps
            tfile.header.ddspf.dwSize = 32
            tfile.header.ddspf.dwSize = 0x20
            tfile.header.dwCaps = 0x400000 + 0x1000 + 0x8
            if tex_dict[mode]:
                # Compressed Textures
                tfile.header.dwPitchOrLinearSize = 0
                tfile.header.ddspf.dwFourCC = tex_dict[mode][0]
                tfile.header.ddspf.dwFlags = 0x4
            else:
                # Uncompressed Textures
                tfile.header.dwPitchOrLinearSize = height * width * 4
                tfile.header.dwFlags += 0x8
                tfile.header.ddspf.dwFourCC = 0
                tfile.header.ddspf.dwFlags = 0x40 + 0x1
                tfile.header.ddspf.dwRGBBitCount = 32
                tfile.header.ddspf.dwRBitMask = 0xFF000000
                tfile.header.ddspf.dwGBitMask = 0x00FF0000
                tfile.header.ddspf.dwBBitMask = 0x0000FF00
                tfile.header.ddspf.dwABitMask = 0x000000FF

            t.close()
            path = os.path.join(dir, name)
            if not os.path.exists(path):
                os.makedirs(path)

            filename = os.path.join(path, 'tex_' + str(i) + '.dds')
            t = open(filename, 'wb')
            t.write(tfile.write_texture().read())
            t.close()
            # Load texture into blender
            bpy.data.images.load(filename)


# Mesh Creation
# Create Mesh with Multiple Parts


def createmeshBuf(verts, startid, faces, uvs, name, count, id, subname, colors, normal_flag, normals, location):
    scn = bpy.context.scene
    mesh = bpy.data.meshes.new('mesh' + str(count))

    bm = bmesh.new()
    # Add Vertices
    verts_dict = {}
    for i in range(len(verts)):
        v = bm.verts.new(verts[i])
        # bm.verts[-1].normal = Vector((normals[i]))
        v.index = i
        verts_dict[startid] = i
        startid += 1

    # Update Normals
    if colors[0]:
        for i in range(len(verts)):
            bm.verts[i].normal = Vector(
                (colors[0][i][0], -colors[0][i][2], colors[0][i][1]))

    # Add Faces
    for face in faces:
        bm.faces.new((bm.verts[verts_dict[face[0]]],
                      bm.verts[verts_dict[face[1]]],
                      bm.verts[verts_dict[face[2]]]))

    # Check for orphan verts
    orphans = []
    for vert in bm.verts:
        if len(vert.link_faces) == 0:
            orphans.append(vert.index)
    # Sort
    orphans.sort(reverse=True)
    # Remove the vertices
    for o in orphans:
        bm.verts.remove(bm.verts[o])

    bm.faces.index_update()

    # Check for orphan faces
    orphans = []
    for face in bm.faces:
        if face.calc_area() == 0:
            orphans.append(face.index)
    # Sort
    orphans.sort(reverse=True)
    # Remove the faces
    for o in orphans:
        bm.faces.remove(bm.faces[o])

    # Fix UVs
    for i in range(len(uvs)):
        if (uvs[i]):
            uvlayer = bm.loops.layers.uv.new('map' + str(i))
            for f in bm.faces:
                for l in f.loops:
                    l[uvlayer].uv.x = uvs[i][l.vert.index][0]
                    l[uvlayer].uv.y = 1 - uvs[i][l.vert.index][1]

    # Fix Color Maps
    # for i in range(len(colors)):
    #    if colors[i]:
    #        collayer = bm.loops.layers.color.new('col' + str(i))
    #        for f in bm.faces:
    #            for l in f.loops:
    #                l[collayer].r = colors[i][l.vert.index][0]
    #                l[collayer].g = colors[i][l.vert.index][1]
    #                l[collayer].b = colors[i][l.vert.index][2]

    # Pass to mesh
    bm.to_mesh(mesh)
    bm.free()

    object = bpy.data.objects.new(
        '_'.join([name, str(id), str(count), subname]), mesh)

    object.location = location
    # object.rotation_euler[0] = radians(90)

    # Transformation attributes inherited from bounding boxes
    # object.scale=Vector((0.1,0.1,0.1))
    scn.objects.link(object)

    return object.name


def facereadstrip(f, count, length):
    faces = []
    indiceslength = length
    facecount = count - 2
    tdict = {2: 'H', 4: 'I'}
    string = '<3' + tdict[length]

    flag = False
    for i in range(facecount):
        back = f.tell()
        temp = struct.unpack(string, f.read(indiceslength * 3))
        if not (temp[0] == temp[1] or temp[1] == temp[2] or temp[0] == temp[2]):
            if not flag:
                faces.append((temp[0], temp[1], temp[2]))
            else:
                faces.append((temp[2], temp[1], temp[0]))
            flag = not flag
        f.seek(back + indiceslength)

    f.seek(indiceslength * 2, 1)
    return faces


vertex_type_dict = {
    0x7C: 'v',
    0x44: 'v88nu',
    0x40: 'v48nu',
    0x3C: 'v8nu',
    0x38: 'v4nu',
    0x34: 'vnu',
    0x30: 'v48nu',
    0x2C: 'v8nu',
    0x28: 'v',
    0x24: 'vnu',
    0x20: ['h', 'v'],  # I have no idea what the fuck this section is
    0x1C: 'v',
    0x18: 'v',
    0x14: 'hm',
    # 0x18: 'v',


}


def skip_subsection(f):
    # Skipping Geometry Sections
    f.seek(4, 1)
    size = struct.unpack('<I', f.read(4))[0]
    f.seek(size - 8, 1)

# Nami
# file = 'COS_014_039_002_264.bin'
# Lucy
# file = 'COS_001_031_001_280.bin'
# Zoro
# file = 'COS_012_038_001_262.bin'
# Zoro Other Costume
# file = 'COS_023_001_005_251.bin'
# Chopper
# file = 'COS_026_005_001_256.bin'
# Luffy Adventure??
# file = 'COS_010_037_001_260.bin'
# Luffy Summer Adventure
# file = 'COS_011_037_002_261.bin'
# Sanji
# file = 'COS_013_041_001_265.bin'
# Robin
# file = 'COS_015_043_002_267.bin'
# Hancock
# file = 'COS_016_010_003_269.bin'
# Perona
# file = 'COS_017_022_002_271.bin'
# Nami Summer
# file = 'COS_018_039_001_263.bin'
# Robin Summer
# file = 'COS_019_043_001_266.bin'
# Hancock Summer
# file = 'COS_020_010_002_268.bin'
# Perona
# file = 'COS_021_022_001_270.bin'
# Luffy Samurai
# file = 'COS_022_000_007_250.bin'
# Sanji Robe
# file = 'COS_024_004_001_255.bin'
# Usopp
# file = 'COS_025_003_003_254.bin'
# Law
# file = 'COS_002_026_002_278.bin'
# Nami
# file = 'COS_003_039_003_273.bin'
# Robin --Investigate Body Parts--
# file = 'COS_004_043_003_275.bin'
# Hancock
# file = 'COS_005_010_004_276.bin'
# Perona
# file = 'COS_006_022_003_277.bin'
# Nami Dress
# file = 'COS_007_002_008_274.bin'
# Tashigi
# file = 'COS_008_028_002_279.bin'
# Shanks
# file = 'COS_009_036_001_281.bin'
# Smoker
# file = 'COS_002_026_002_278.bin'
file = 'oppw3_A_file_747_nami'
texfile = 'oppw3_A_file_1486_textures'
#texfile = None
f = open(os.path.join(dir, file), 'rb')

dlc_flag = False

if dlc_flag:
    # Start of the file contains a section with offsets

    # File Header is 0x10 Bytes
    f.read(4)  # Skip 0x64000000
    offset_count = struct.unpack('<I', f.read(4))[0]
    f.read(8)

    # Immediately get mesh_offset
    f.seek(0x30, 1)
    mesh_offset = struct.unpack('<I', f.read(4))[0]

    # Get Texture Offset
    tex_offset = struct.unpack('<I', f.read(4))[0]

    print('-----------------------------------------')
    print('Mesh Offset: ', mesh_offset)

    f.seek(tex_offset)
    parseTextures(f, file)

else:
    print('Non DLC model')
    if texfile:
        print('Parsing Textures')
        path = os.path.join(dir, texfile)
        t = open(path, 'rb')
        parseTextures(t, path)
        t.close()
        print('Continuing with Model')
    mesh_offset = 0x0

# Seek to mesh offset
f.seek(mesh_offset)

# This Section contains offsets for the models
mesh_count = struct.unpack('<I', f.read(4))[0] - 1
f.seek(8, 1)  # Skipping size and first offset
mesh_offsets = struct.unpack(
    '<' + (mesh_count - 1) * 'I', f.read((mesh_count - 1) * 4))

object_list = []
valid = []

# Iterate in meshes
gmigcount = 0
count = 0
for off in mesh_offsets:
    f.seek(mesh_offset + off)
    # First Section M1G
    main_name = read_string_n(f, 8)
    main_size = struct.unpack('<I', f.read(4))[0]
    main_off = struct.unpack('<I', f.read(4))[0]
    f.seek(mesh_offset + off + main_off)
    # Second Section FM1G
    fmig_name = read_string_n(f, 8)
    fmig_size = struct.unpack('<I', f.read(4))[0]
    f.seek(0x8, 1)  # Skipping Some Other Numbers
    smig_count = struct.unpack('<I', f.read(4))[0]

    f.seek(mesh_offset + off + main_off + fmig_size)
    # print('testoff', hex(mesh_offset + off + main_off + fmig_size))
    smig_fullsize = 0
    for i in range(smig_count):
        smig_name = read_string_n(f, 8)
        smig_size = struct.unpack('<I', f.read(4))[0]
        f.seek(smig_size - 0xC, 1)
        smig_fullsize += smig_size

    # Third Section 1st SM1G
    # smig1_name = read_string_n(f, 8)
    # smig1_size = struct.unpack('<I', f.read(4))[0]
    # f.seek(mesh_offset + off + main_off + fmig_size + smig1_size)
    # Fourth Section 2nd SM1G
    # smig2_name = read_string_n(f, 8)
    # smig2_size = struct.unpack('<I', f.read(4))[0]
    # f.seek(mesh_offset + off + main_off + fmig_size + smig1_size +
    # smig2_size)

    # Fifth Section MM1G
    mmig_name = read_string_n(f, 8)
    mmig_size = struct.unpack('<I', f.read(4))[0]
    f.seek(mesh_offset + off + main_off + fmig_size +
           smig_fullsize + mmig_size)
    # Sixth Section GM1G
    gmig_name = read_string_n(f, 8)
    gmig_size = struct.unpack('<I', f.read(4))[0]
    dx_version = read_string_n(f, 3)
    f.seek(0x5, 1)  # Skipping some zeroes
    f.seek(0x18, 1)  # Skipping some Values
    print(gmigcount, gmig_name, hex(mesh_offset + off))

    subection_count = struct.unpack('<I', f.read(4))[0]
    # Skipping Subsections
    skip_subsection(f)  # First Subsection
    skip_subsection(f)  # Second Subsection
    # Second Section contains double the entries of the vertices sections
    # There are 7 floats in each part.
    skip_subsection(f)  # Third Subsection Model Options
    # Third section is empty most of the time

    # Fourth Subsection Contains the vertex Data
    vbuffers = []
    valid = []

    f.seek(0x8, 1)  # Skipping header and size
    vertex_sections_count = struct.unpack('<I', f.read(4))[0]
    for i in range(vertex_sections_count):
        # 10 Bytes Header
        f.seek(4, 1)  # Skipping zeroes
        vertex_type = struct.unpack('<I', f.read(4))[0]
        vertex_count = struct.unpack('<I', f.read(4))[0]
        f.seek(4, 1)  # Skipping zeroes

        vertex_offset = f.tell()
        # Storing Geometry Buffer Separately
        t = BytesIO()
        t.write(f.read(vertex_type * vertex_count))
        t.seek(0)
        vbuffers.append(t)
        valid.append(
            [vertex_type in vertex_type_dict, vertex_type, vertex_offset])

    print('5th section: ', hex(f.tell()))
    skip_subsection(f)  # Fifth Subsection Some per vertex buffer information
    skip_subsection(f)  # Sixth Subsection Some useless shit here

    # Seventh Section Contains Indices Streams
    ibuffers = []
    f.seek(0x8, 1)  # Skipping header and size
    indices_sections_count = struct.unpack('<I', f.read(4))[0]
    for i in range(indices_sections_count):
        # 0xC Bytes Header
        indices_count = struct.unpack('<I', f.read(4))[0]
        indices_type = struct.unpack('<I', f.read(4))[0]
        f.seek(4, 1)  # Skipping Zeroes
        if indices_type == 0x10:  # 0x10=16 Probably means 16 bits indices
            t = BytesIO()
            t.write(f.read(indices_count * 2))
            # t.close()
            ibuffers.append(t)
        else:
            print('Unsupported Indices Type')
    print('Vertex Sections in Part: ', vertex_sections_count)
    print('Indices Sections in Part: ', indices_sections_count)

    # 8th Section contains object sections
    print('8th section: ', hex(f.tell()))
    f.seek(0x8, 1)  # Skip Header and Size
    object_count = struct.unpack('<I', f.read(4))[0]
    # Parsing in 0x38 sections
    oldId = 0
    #face_parts = []
    for i in range(object_count):
        id = struct.unpack('<I', f.read(4))[0]  # 0x35000000
        bufferId = struct.unpack('<I', f.read(4))[0]
        partId = struct.unpack('<I', f.read(4))[0]
        # Find vertex_type and Offset
        vertex_type = valid[bufferId][1]
        vertex_offset = valid[bufferId][2]

        partData = struct.unpack('<3I', f.read(0xC))
        materialId = struct.unpack('<I', f.read(4))[0]
        print('part: ', i, 'buffer', bufferId, 'info',
              'vertex_type', hex(vertex_type), 'off', hex(vertex_offset), partData, 'material', materialId)

        f.seek(0xC, 1)

        # Don't need the vertices counts
        vstart = struct.unpack('<I', f.read(4))[0]
        vcount = struct.unpack('<I', f.read(4))[0]
        # f.seek(0x8, 1)
        istart = struct.unpack('<I', f.read(4))[0]
        icount = struct.unpack('<I', f.read(4))[0]
        print(vstart, vcount, istart, icount)
        # Get verts
        t = vbuffers[bufferId]
        t.seek(vstart * vertex_type)

        vtemp = []
        ntemp = []
        uvtemp = []

        # Check for anomalies
        if vertex_type == 0x20:
            if partData[0] < 3:
                fmt = vertex_type_dict[vertex_type][0]
            else:
                fmt = vertex_type_dict[vertex_type][1]
        else:
            fmt = vertex_type_dict[vertex_type]

        for j in range(vcount):
            buf = 0

            for opt in fmt:
                if opt == 'v':
                    val = struct.unpack('<3f', t.read(0xC))
                    vtemp.append((val[0], val[2], val[1]))
                    buf += 0xC
                elif opt == 'h':
                    vx = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    vy = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    vz = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    t.seek(0x2, 1)
                    hx = struct.unpack('f', struct.pack('I', vx))[0]
                    hy = struct.unpack('f', struct.pack('I', vy))[0]
                    hz = struct.unpack('f', struct.pack('I', vz))[0]
                    vtemp.append((hx, hz, hy))
                    buf += 0x8
                elif opt == 'm':
                    vx = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    vy = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    vz = hf.decompress(struct.unpack('<H', t.read(2))[0])
                    t.seek(0x2, 1)
                    hx = struct.unpack('f', struct.pack('I', vx))[0]
                    hy = struct.unpack('f', struct.pack('I', vy))[0]
                    hz = struct.unpack('f', struct.pack('I', vz))[0]
                    ntemp.append((hx, hz, hy))
                    buf += 0x8
                elif opt == 'u':
                    uvtemp.append(struct.unpack('<2f', t.read(0x8)))
                    buf += 0x8
                elif opt == 'n':
                    val = struct.unpack('<3f', t.read(0xC))
                    ntemp.append((val[0], val[1], val[2]))
                    t.seek(0x4, 1)
                    buf += 0x10
                else:
                    t.seek(int(opt), 1)
                    buf += int(opt)

            t.seek(vertex_type - buf, 1)

        # Append Indices
        t = ibuffers[bufferId]
        t.seek(istart * 2)
        part = facereadstrip(t, icount, 2)

        # Create Mesh
        obname = createmeshBuf(vtemp, vstart, part, [uvtemp],
                               'oppw3', gmigcount, '_'.join([str(hex(vertex_type)),
                                                             'v' + str(hex(vertex_offset))]),
                               str(partId), [ntemp], False, [], Vector((0.0, 0.0, 0.0)))
        object_list.append((obname, i))

        # Create and/or assign material
        matname = 'oppw3_' + str(gmigcount) + '_' + str(materialId)
        ob = bpy.data.objects[obname]
        if matname in bpy.data.materials:
            # Assign Existing Material
            mat = bpy.data.materials[matname]
            ob.data.materials.append(mat)
        else:
            # Create Material
            mat = bpy.data.materials.new(matname)
            # Use Nodes for Cycles Rendering\
            mat.use_nodes = True
            nodetree = mat.node_tree
            diffnode = nodetree.nodes['Diffuse BSDF']

            tx = nodetree.nodes.new('ShaderNodeTexImage')
            nodetree.nodes[-1].name = 'Diffuse Tex'
            nodetree.links.new(diffnode.inputs['Color'], tx.outputs['Color'])

            tx = nodetree.nodes.new('ShaderNodeTexImage')
            nodetree.nodes[-1].name = 'Normal Tex'
            nodetree.links.new(diffnode.inputs['Normal'], tx.outputs['Color'])

            # Assign material
            ob.data.materials.append(mat)

    skip_subsection(f)  # 9th Subsection
    gmigcount += 1

    # Close open files
    for t in ibuffers:
        t.close()
    for t in vbuffers:
        t.close()
    gc.collect()

# Recalculate Normals
for name, id in object_list:
    ob = bpy.data.objects[name]
    # print('Normalizing')
    bpy.context.scene.objects.active = ob
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all()
    bpy.ops.mesh.normals_make_consistent()
    bpy.ops.mesh.faces_shade_smooth()
    bpy.ops.object.mode_set(mode='OBJECT')

f.close()
