import os
import sys
import time
import struct
from string_func import *
from dds30 import *
from io import BytesIO as StringIO


tex_dict = {0x1: False,
            0x6: ['DXT1', 8],
            0x8: ['DXT5', 16]}


def parseTextures(f, name):
    baseOffset = f.tell()
    identifier = read_string_n(f, 8)
    fileSize = struct.unpack('<I', f.read(4))[0]
    main_offset = struct.unpack('<I', f.read(4))[0]
    textureCount = struct.unpack('<I', f.read(4))[0]
    name += '_textures'

    texOffsets = []
    sizes = []
    f.seek(baseOffset + main_offset)
    for i in range(textureCount):
        texOffsets.append(struct.unpack('<I', f.read(4))[0] + main_offset)

    for i in range(textureCount - 1):
        sizes.append(texOffsets[i + 1] - texOffsets[i])
    sizes.append(fileSize - texOffsets[-1])

    for i in range(textureCount):
        of = texOffsets[i]
        size = sizes[i]
        if of < fileSize:
            # print 'off: ', of
            f.seek(baseOffset + of)
            # Identify texture
            texinfo = struct.unpack('<4B', f.read(4))
            mipmaps = texinfo[0] >> 4
            mode = texinfo[1]
            width = 2 ** (texinfo[2] & 0x0F)
            height = 2 ** ((texinfo[2] & 0xF0) >> 4)

            head = struct.unpack('<2H', f.read(4))
            if head[1] == 0x21:
                # Decompressed Texture
                size -= 8
            elif head[1] == 0x1021:
                # Compressed Texture
                f.seek(0xC, 1)
                size -= 16
            print(hex(of), hex(size), [mipmaps, mode, width, height])

            # Create DDS File
            t = StringIO()
            t.write(f.read(size))
            t.seek(0)
            tfile = dds_file(False, t)
            # Set parameters
            tfile.header.dwSize = 0x7C
            tfile.header.dwFlags = 0x20000 + 0x1000 + 0x1 + 0x2 + 0x4
            tfile.header.dwHeight = height
            tfile.header.dwWidth = width
            tfile.header.dwMipMapCount = mipmaps
            tfile.header.ddspf.dwSize = 32
            tfile.header.ddspf.dwSize = 0x20
            tfile.header.dwCaps = 0x400000 + 0x1000 + 0x8
            if tex_dict[mode]:
                # Compressed Textures
                tfile.header.dwPitchOrLinearSize = 0
                tfile.header.ddspf.dwFourCC = tex_dict[mode][0]
                tfile.header.ddspf.dwFlags = 0x4
            else:
                # Uncompressed Textures
                tfile.header.dwPitchOrLinearSize = height * width * 4
                tfile.header.dwFlags += 0x8
                tfile.header.ddspf.dwFourCC = 0
                tfile.header.ddspf.dwFlags = 0x40 + 0x1
                tfile.header.ddspf.dwRGBBitCount = 32
                tfile.header.ddspf.dwRBitMask = 0xFF000000
                tfile.header.ddspf.dwGBitMask = 0x00FF0000
                tfile.header.ddspf.dwBBitMask = 0x0000FF00
                tfile.header.ddspf.dwABitMask = 0x000000FF

            t.close()
            path = os.path.join(dir, name)
            if not os.path.exists(path):
                os.makedirs(path)

            filename = os.path.join(path, 'tex_' + str(i) + '.dds')
            t = open(filename, 'wb')
            t.write(tfile.write_texture().read())
            t.close()


dir = 'L:\\Users\\gregkwaste\\Documents\\Projects\\OPPW3\\A file Extracted'

for i in os.listdir(dir):
    path = os.path.join(dir, i)
    if 'textures' in path:
        print path
        f = open(path, 'rb')
        parseTextures(f, i)
        time.sleep(1)
        f.close()

    # try:
    #    identifier = ''.join(struct.unpack('<8c', f.read(8)))
    # except:
    #    f.close()
    #    continue
    # f.close()
    # if identifier == 'GT1G0600':
    #    print i, 'has textures'
    #    if 'textures' not in i:
    #        os.rename(path, path + '_textures')
