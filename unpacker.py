import zlib
import struct
import os
import sys
from StringIO import StringIO
import time


def unpack(f, name, size):
    dir = os.path.dirname(name)
    if not os.path.exists(dir):
        os.makedirs(dir)
    k = open(name, 'wb')
    k.write(f.read(size))
    k.close()


def unzliber(f, name):

    start_off = f.tell()
    decomp_size = struct.unpack('<I', f.read(4))[0]

    # Start Reading Parts
    t = StringIO()
    count = 0
    comp_size = 0
    fileno = 0
    try:
        while (count < decomp_size):
            psize = struct.unpack('<I', f.read(4))[0]
            comp_size += psize
            if not psize:
                break
            temp = zlib.decompress(f.read(psize))
            count += len(temp)
            fileno += 1
            t.write(temp)
    except:
        sys.stdout.write('Zlib Error ' + hex(start_off) + '\n')
        t.close()
        return

    # print count, decomp_size, comp_size, fileno, hex(start_off)
    t.seek(0)
    # Write File
    dir = os.path.dirname(name)
    if not os.path.exists(dir):
        os.makedirs(dir)
    k = open(name, 'wb')
    k.write(t.read())
    k.close()

    t.close()


# f = open(
#    'I:\\Downloaded Games\\3DMGAME-One.Piece.Pirate.Warriors.3.Incl.DLC.Cracked-3DM\\3DMGAME-One.Piece.Pirate.Warriors.3.Incl.DLC.Cracked-3DM\\OPPW3\\LINKDATA_DX9\\LINKDATA_EU_OS_EUNA.A', 'rb')

def main():
    try:
        filepath = sys.argv[1]
    except:
        sys.stdout.write('Missing Argument' + '\n')
        return

    if not os.path.exists(filepath):
        sys.stdout.write('File does not exist' + '\n')
        return
    sys.stdout.write('One Piece Pirate Warriors 3 Unpacker v1.0' + '\n')
    sys.stdout.write('Coded by gregkwaste' + '\n')
    dir = os.path.dirname(filepath)
    filename = os.path.basename(filepath)
    f = open(filepath, 'rb')
    header = struct.unpack('<4I', f.read(0x10))
    # print header

    file_list = []
    data_offset = ((((header[1] + 1) * 0x10) / 0x800) + 1) * 0x800
    pre_offset = ((((header[1] + 1) * 0x10) / 0x800) + 1) * 0x800
    sys.stdout.write('Init Data Offset: ' + str(data_offset) + '\n')

    t = open('file_list.txt', 'w')
    for i in range(header[1]):
        file_head = struct.unpack('<4I', f.read(0x10))
        file_offset = pre_offset
        file_list.append((file_head, file_offset))
        t.write(str(file_head) + '_offset_' +
                str(file_offset) + '_id' + str(i) + '\n')
        mod = file_head[2] % 0x800
        if mod:
            pre_offset = file_offset + file_head[2] + (0x800 - mod)
        else:
            pre_offset = file_offset + max(file_head[2], 0x800)

    t.close()

    fileno = 0
    # Store Files
    dir = os.path.join(dir, filename + '_files')
    for head, of in file_list:
        f.seek(of)
        filename = os.path.join(dir, 'file_' + str(fileno))
        sys.stdout.write(filename + '\n')

        if head[3]:
            # Unzlib the file if compressed
            unzliber(f, filename)
        else:
            # Simply Extract the file from the Archive
            unpack(f, filename, head[2])
        fileno += 1
        time.sleep(0.005)
    sys.stdout.write('Finished' + '\n')
    f.close()

main()
